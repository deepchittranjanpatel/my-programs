/*Following C++ program finds prime numbers from 1 to 100 using Method of Sieve of Erasthoneus(Compiled using Turbo C++)
  You can modify the program according to your needs, but the general logic remains same-By Deep C. Patel */
#include<iostream.h>
#include<conio.h>
#define MAX 100   //You Can Change the limit to more than 100 also

void main()
{
	clrscr();

	int ar[MAX],c=0, a=MAX, k=0, ar1[MAX], ar2[MAX], h, small,j=10000, m, l=0, s=0;

	h=a;
	ar[0]=1; ar2[0]=1;       //Array ar1[] will store prime nos. and ar2[] and ar[] will store nos. 1 to 100

	for(int i=0; i<a; i++)
	{
		ar2[i]=i+1;
		ar[i]=i+1;

	}

	cout<<"\n\nThe Array has Elements as Shown Below:-\n";
	for(i=0;i<a;i++)
	{
		cout<<"\n"<<ar[i];
	}

	while(j)
	{
		j--;
		l++;
		m=1;
		c=small;
		small=ar2[1];

		for(i=l;i<h;i++)
		{
			if(ar2[i]<small)
			{
				small=ar2[i];
			}
		}


		if(small==c)
		{
			break;
		}

		ar1[k]=small;
		k++;


		for(i=1;i<h;i++)
		{
			if((ar2[i]%small)!=0)
			{
				ar2[m]=ar2[i];
				m++;
			}
		}

		h=m+1;

		s++;

	}

	cout<<"\n\nThe Prime Numbers are:-\n";

	for(i=0;i<k;i++)
	{
		cout<<"\n"<<ar1[i];
	}

	cout<<"\n\nThank You";

	getch();
}


