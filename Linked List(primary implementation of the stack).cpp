#include<iostream>
#include<process.h>  // for exit()
#include<conio.h>
#include<string.h>
#include<stdlib.h>


using namespace std;

class node
{
    public:
	int info;
	int rollno;
	char name[20];
	node* next;
} *start,*newptr,*save,*ptr;

node* Create_New_Node(int, int, char*);
void insertbeg(node*);
void Display(node*);
void dt(node*);


int main()
{
	start=NULL;
	int inf, rollno;
	char n[20];
	char ch='y';
	while(ch=='y' || ch=='Y')
	{

        system("cls");

        cout<<"\nDo you want to delete the Node(y/n):";
		cin>>ch;
		if(ch=='y'||ch=='Y')
        {
            dt(start);
        }

		cout<<"\n\nEnter info for the New Node:";
		cin>>inf;
		cout<<"\nEnter roll no for new node:";
		cin>>rollno;
		cout<<"\nEnter the name:";
		cin>>n;
		cout<<"\nCreating new node, Press Enter";
		getch();

		newptr=Create_New_Node(inf, rollno, n);

		if(newptr!=NULL)
		{
            cout<<"\nNew node created successfully, Press Enter";
            getch();
		}
		else
		{
            cout<<"Unsuccessful in creating the Node"<<endl;
            getch();
		}
		cout<<"\nNow inserting this new node in the beginning of list\n";
		getch();

		insertbeg(newptr);

		cout<<"\nNow the list is \n";
		Display(start);

		cout<<"\nDo you want to delete the Node(y/n):";
		cin>>ch;
		if(ch=='y'||ch=='Y')
        {
            dt(start);
        }

        cout<<"\nNow the list is \n";
		Display(start);

		cout<<"\nPress y to enter new nodes, N to exit...\n:";
		cin>>ch;
	}
	return 0;
}

node* Create_New_Node(int n, int r, char*c)
{
  ptr=new node;
  ptr->info=n;
  ptr->rollno=r;
  strcpy(ptr->name,c);
  ptr->next=NULL;
  return ptr;
}

void insertbeg(node*np)
{
	if(start==NULL)
	start=np;

	else
	{
		save=start;
		start=np;
		np->next=save;
	}
}

void Display(node*np)
{
	while(np!=NULL)
	{
		cout<<np->info<<","<<np->rollno<<","<<np->name<<"->";
		np=np->next;
	}
	cout<<"\n";
}

void dt(node*np)
{
    start=np;
    if(start==NULL)
    {
        cout<<"\nNo node Exists to delete";
    }
    else
    {
        start=start->next;
    }

}
